import { useEffect, useState } from "react";

export default function Stopwatch() {
  const [time, setTime] = useState({
    s: 0,
    m: 0,
    h: 0,
  });

  const [timeSeconds, setTimeSeconds] = useState(0);
  const [isRunning, setIsRunning] = useState(false)
  const start = () => {
    console.log("start")
    setIsRunning(true)
  }

  const stop = () => {
    setIsRunning(false)
  }

  const reset = () => {
    setIsRunning(false)
    setTimeSeconds(0)
  }

  useEffect(() => {
    let timer: any
    if(isRunning) {
      console.log('Running')
      timer = setInterval(() => {
        console.log("Tick")
        setTimeSeconds(prev => {
          if(prev < 1000000) {
            return prev + 1
          } else {
       
            clearInterval(timer)
            return 0
          }
        })
      }, 1000)
    } else{
      clearInterval(timer)
    }

    return () => clearInterval(timer)
  }, [isRunning])

  useEffect(() => {
    setTime(secondsToTimeObject(timeSeconds))
  }, [timeSeconds])

  const TimeView = () => (<>
    <p className="text-5xl text-center">
      {time.h > 0 && <span>{time.h}<small className="text-[24px]">h</small></span>}
       <span className="text-gray-600">{time.m > 10 ? time.m : '0'+time.m}:</span>
      <span>{time.s > 10 ? time.s : '0'+time.s}<small className="text-[24px]"></small></span>
    </p>
  </>)
  return (
    <div className="min-w-max min-h-[350px] flex items-center justify-center">
      <div>
        <TimeView />
        <div className="mt-6">
        <button
          onClick={() => !isRunning ? start() : stop()}
          type="button"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Start/Stop</button>
        <button
          onClick={() => reset()}
          type="button" className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Reset</button>
        </div>
      </div>
    </div>
  );
}

const secondsToTimeObject = (seconds: number) => {
    const h: number = Math.floor(seconds/3600)
    const m: number = Math.floor((seconds - (h * 3600)) /60)
    const s: number = seconds - (h * 3600) - (m *60)
    return {
      h, m, s
    }
}

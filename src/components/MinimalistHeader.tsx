import Image from "next/image";

export default function MinimalistHeader() {
  return (
    <>
      <div
        className="bg-white rounded-t-full mt-2 h-0 md:h-8"
      ></div>
      <nav className="bg-white py-6">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="flex items-center justify-between">
            <a href="/" aria-label="Home">
              <Image
                src="/logo-minimalist.png"
                width={120}
                height={80}
                className="mr-3 h-12"
                alt="Logo"
              />
            </a>
            <div className="flex items-center gap-x-8">
              <a
                className="inline-flex rounded-full px-4 py-1.5 text-sm font-semibold transition bg-neutral-950 text-white hover:bg-neutral-800"
                href="/contact"
              >
                <span className="relative top-px">Contact us</span>
              </a>
              <button
                type="button"
                aria-expanded="false"
                aria-controls=":R3a:"
                className="group -m-2.5 rounded-full p-2.5 transition hover:bg-neutral-950/10"
                aria-label="Toggle navigation"
              >
                <svg
                  viewBox="0 0 24 24"
                  aria-hidden="true"
                  className="h-6 w-6 fill-neutral-950 group-hover:fill-neutral-700"
                >
                  <path d="M2 6h20v2H2zM2 16h20v2H2z"></path>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}

import Link from "next/link";
import { ReactNode } from "react";

interface CardButtonProps {
  link?: string;
  children?: ReactNode;
  className?: string;
}

export default function CardButton({ link, children, className }: CardButtonProps) {
  return (
    <div className={"grow min-w-[200px] max-w-[400px]"}>
      <a href={link ?? "#"}>
        <div className="px-4 py-10 shadow-l hover:shadow-xl transition ease-in-out delay-150 border-solid border-2 border-slate-200 rounded-xl">
          <p className="font-medium mx-auto text-center">{children}</p>
        </div>
      </a>
    </div>
  );
}

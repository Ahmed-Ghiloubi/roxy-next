import Image from "next/image";
import Link from "next/link";

export default function About() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-slate-600">
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          <code className="font-mono font-bold">Roxy Tools Privacy</code>
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
          <Link
            className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
            href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            By{" "}
            <Image
              src="https://roxytools.com/logo.png"
              alt="Roxy-Tools Logo"
              className="dark:invert"
              width={100}
              height={24}
            />
          </Link>
        </div>
      </div>

      <div className="relative p-20 flex flex-col place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 before:lg:h-[360px]">
        <h1 className="text-xl mb-8">
          Privacy Policy for Roxy Tools Android Apps
        </h1>
        <div>
          <h3>Effective Date: 16/05/2023</h3>

          <p className="mb-6">
            Thank you for using Roxy Tools Android Apps (&quot;the Apps&quot;) developed
            by Roxy Tools. This Privacy Policy outlines how we collect, use,
            and disclose your personal information when you use our Apps. We are
            committed to protecting your privacy and ensuring the security of
            your personal information.
          </p>

          <h2 className="text-xl">1. Information We Collect</h2>

          <h3 className="text-l">1.1 Personal Information</h3>
          <p className="mb-6">
            When you use our Apps, we may collect certain personal information
            that you provide to us voluntarily, such as your name, email
            address, and contact details. We only collect personal information
            necessary for the functioning of the Apps and to provide you with
            the best user experience.
          </p>

          <h3>1.2 Usage Information</h3>
          <p className="mb-6">
            We may collect non-personal information about your interactions with
            the Apps, including but not limited to the pages visited, features
            used, and the duration and frequency of your activities within the
            Apps. This information is collected to help us improve the
            performance and functionality of the Apps.
          </p>

          <h2>2. Use of Information</h2>

          <h3>2.1 Provide and Improve the Apps</h3>
          <p className="mb-6">
            We may use the collected information to operate, maintain, and
            enhance the features and functionality of the Apps. This includes
            providing personalized content, responding to your inquiries, and
            improving user experience.
          </p>

          <h3>2.2 Communication</h3>
          <p className="mb-6">
            We may use your email address or contact details to communicate with
            you regarding updates, new features, promotions, or important
            notices related to the Apps. You can opt-out of receiving such
            communications by following the instructions provided in the
            communication.
          </p>

          <h3>2.3 Aggregate Data</h3>
          <p className="mb-6">
            We may use anonymized and aggregated data for analytical purposes,
            such as monitoring App usage trends, conducting research, and
            generating statistical information. This data does not identify any
            individual and is used to improve our services.
          </p>

          <h2>3. Disclosure of Information</h2>

          <h3>3.1 Service Providers</h3>
          <p className="mb-6">
            We may engage third-party service providers or contractors who
            assist us in providing and maintaining the Apps. These service
            providers may have access to your personal information only to
            perform tasks on our behalf and are obligated not to disclose or use
            it for any other purpose.
          </p>

          <h3>3.2 Legal Requirements</h3>
          <p className="mb-6">
            We may disclose your personal information if required by law,
            regulation, legal process, or governmental request. We will also
            disclose information if we believe it is necessary to protect our
            rights, property, or safety, or the rights, property, or safety of
            others.
          </p>

          <h2>4. Data Security</h2>

          <p className="mb-6">
            We take reasonable measures to protect your personal information
            from unauthorized access, alteration, disclosure, or destruction.
            However, no method of transmission over the internet or electronic
            storage is 100% secure, and we cannot guarantee absolute security.
          </p>

          <h2>5. Children&apos;s Privacy</h2>

          <p className="mb-6">
            The Apps are not intended for use by individuals under the age of
            13.
          </p>

          <h2>6. Third-Party Links and Services</h2>
          <p className="mb-6">
            The Apps may contain links to third-party websites or services. We
            are not responsible for the privacy practices or the content of
            these third-party websites or services. We encourage you to read the
            privacy policies of those third parties before providing any
            personal information.
          </p>
          <h2>7. Changes to the Privacy Policy</h2>
          <p className="mb-6">
            We may update this Privacy Policy from time to time. Any changes we
            make will be effective when we post the revised Privacy Policy on
            the Apps. We encourage you to review this Privacy Policy
            periodically for any updates or changes.
          </p>
          <h2>8. Contact Us</h2>
          <p className="mb-6">
            If you have any questions, concerns, or requests regarding this
            Privacy Policy or our privacy practices, please contact us at
            [Contact Email].
          </p>
          <p className="mb-6">
            By using our Apps, you consent to the collection, use, and
            disclosure of your personal information as described in this Privacy
            Policy.
          </p>
        </div>
      </div>

      <div className="mb-32 grid text-center lg:mb-0 lg:grid-cols-4 lg:text-left">
        <Link
          href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Docs{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Find in-depth information about Next.js features and API.
          </p>
        </Link>

        <Link
          href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template-tw&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800 hover:dark:bg-opacity-30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Learn{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Learn about Next.js in an interactive course with&nbsp;quizzes!
          </p>
        </Link>

        <Link
          href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Templates{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Explore the Next.js 13 playground.
          </p>
        </Link>

        <Link
          href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Deploy{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Instantly deploy your Next.js site to a shareable URL with Vercel.
          </p>
        </Link>
      </div>
      <style>
        {`
        body {
          color: rgb(var(--foreground-rgb));
          background: linear-gradient(
              to bottom,
              transparent,
              rgb(var(--background-end-rgb))
            )
            rgb(var(--background-start-rgb));
        }
        `}
      </style>
    </main>
  );
}

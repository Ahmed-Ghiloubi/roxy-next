import CardButton from "@/components/CardButton";

export default function HomePage() {
  return (
    <>
      {/* Slider */}
      <div className="w-full flex-auto">
        <div className="mx-auto max-w-7xl px-6 lg:px-8 mt-16">
          <h1 className="font-display text-5xl font-medium tracking-tight text-neutral-950 [text-wrap:balance] sm:text-7xl">
            Web Tools that will improve your productivity.
          </h1>
          <p className="mt-6 text-xl text-neutral-600">
            Enhance Productivity with Web Tools: Simplify tasks and optimize
            efficiency for increased effectiveness
          </p>
        </div>
      </div>
      {/* Services */}
      <div className="mt-20 mb-20 mx-auto max-w-7xl px-6 lg:px-8">
      <h1 className="h1 text-3xl font-display font-bold mb-6">Services</h1>
        <div className="mb-6">
          <h2 className="h2 text-l font-bold">Apps</h2>
          <p className="mb-2">
            Usefull apps to help you in your daily life/work
          </p>
          <div className="flex flex-wrap gap-2">
            <CardButton className="grow" link={"/apps/timer-stopwatch"}>Timer / Stop Watch</CardButton>
            <CardButton className="grow" link={"#" || "/apps/html-viewer"}>Live HTML Viewer</CardButton>
            <CardButton className="grow" link={"#" || "/apps/json-formatter"}>Json formatter</CardButton>
            <CardButton className="grow" link={"#" || "/my-ip"}>My IP</CardButton>
            <CardButton className="grow" link={"#" || "/apps/color-picker"}>Color picker</CardButton>
          </div>
        </div>
        <div>
          <h2 className="h2 text-l font-bold">Tools</h2>
          <p>Converters, Encoders/Decoders and other usefull tools</p>
          <div className="flex flex-wrap gap-2">
            <CardButton link="/tools/jwt-decoder">JWT decoder</CardButton>
            <CardButton link={"/tools/url-encode-decode"}>
              URL Encode/Decode
            </CardButton>
            <CardButton link={"/tools/base64-converter"}>
              Base64 Converter
            </CardButton>
            <CardButton link={"#" || "/tools/hex-dec-converter"}>
              Hex/Dec Converter
            </CardButton>
            <CardButton link={"#" || "/tools/md5-hash-generator"}>
              MD5 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha1-hash-generator"}>
              sha-1 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha256-hash-generator"}>
              sha-256 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha512-hash-generator"}>
              sha-512 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/random-number-generator"}>
              Random number Generator
            </CardButton>
          </div>
        </div>
      </div>
    </>
  );
}

import Header from "@/components/Header";
import CardButton from "@/components/CardButton";

export default function Home() {
  return (
    <>
      <Header></Header>
      <main className="min-h-screen p-16 container mx-auto">
        <h1 className="h1 text-lg font-bold mb-6">Roxy Tools</h1>
        <div className="mb-6">
          <h2 className="h2 text-l font-bold">Apps</h2>
          <p className="mb-2">
            Usefull apps to help you in your daily life/work
          </p>
          <div className="flex gap-2">
            <CardButton link={"#" || "/apps/timer"}>Timer / Stop Watch</CardButton>
            <CardButton link={"#" || "/apps/html-viewer"}>Live HTML Viewer</CardButton>
            <CardButton link={"#" || "/apps/json-formatter"}>Json formatter</CardButton>
            <CardButton link={"#" || "/my-ip"}>My IP</CardButton>
            <CardButton link={"#" || "/apps/color-picker"}>Color picker</CardButton>
          </div>
        </div>
        <div>
          <h2 className="h2 text-l font-bold">Tools</h2>
          <p>Converters, Encoders/Decoders and other usefull tools</p>
          <div className="flex gap-2">
            <CardButton link="/tools/jwt-decoder">JWT decoder</CardButton>
            <CardButton link={"/tools/url-encode-decode"}>
              URL Encode/Decode
            </CardButton>
            <CardButton link={"/tools/base64-converter"}>
              Base64 Converter
            </CardButton>
            <CardButton link={"#" || "/tools/hex-dec-converter"}>
              Hex/Dec Converter
            </CardButton>
            <CardButton link={"#" || "/tools/md5-hash-generator"}>
              MD5 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha1-hash-generator"}>
              sha-1 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha256-hash-generator"}>
              sha-256 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/sha512-hash-generator"}>
              sha-512 Hash Generator
            </CardButton>
            <CardButton link={"#" || "/tools/random-number-generator"}>
              Random number Generator
            </CardButton>
          </div>
        </div>
      </main>
    </>
  );
}

"use client";
import Header from "@/components/Header";
import jwt_decode from "jwt-decode";
import { useState } from "react";

export default function JwtDecoderPage() {
  const [decoded, setDecoded] = useState("");
  const [encoded, setEncoded] = useState("");
  const [error, setError] = useState<String | undefined>();

  const decode = () => {
    setError("");
    try {
      setDecoded(JSON.stringify(jwt_decode(encoded), null, 2));
    } catch (e) {
      if (encoded.length === 0) {
        setError('Insert the JWT')
      } else {
        setError("Invalid Token");
      }
    }
  };

  return (
    <>
      <main className="mx-auto max-w-7xl px-6 lg:px-8 mt-16">
        <div>
          <h2 className="text-3xl font-medium mb-6">JWT decoder</h2>
          <div className="mb-4">
            <label>Algorithm</label>
            <select className="" id="algorithm-select" name="algorithm" defaultValue={"HS256"}>
              <option value="HS256">
                HS256
              </option>
              <option value="HS384">HS384</option>
              <option value="HS512">HS512</option>
              <option value="RS256">RS256</option>
              <option value="RS384">RS384</option>
              <option value="RS512">RS512</option>
              <option value="ES256">ES256</option>
              <option value="ES384">ES384</option>
              <option value="ES512">ES512</option>
              <option value="PS256">PS256</option>
              <option value="PS384">PS384</option>
              <option value="PS512">PS512</option>
            </select>
          </div>
          <div className="flex gap-4">
            <div className="flex-1 flex flex-col">
              <label htmlFor="encoded-jwt" className="mb-2">
                Encoded JWT
              </label>
              <textarea
                id="encoded-jwt"
                value={encoded}
                onChange={(e) => {setError(''); setEncoded(e.target.value)}}
                className="resize-none border-solid border-2 border-slate-800 h-64 p-2"
                placeholder="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
              ></textarea>
              {error && <p className="text-rose-500">{error}</p>}
              <div>
                <button
                  onClick={decode}
                  type="button"
                  className="mt-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                >
                  Decode {'->'}
                </button>
              </div>
            </div>
            <div className="flex-1 flex flex-col">
              <label htmlFor="decoded-jwt" className="mb-2">
                Decoded JWT
              </label>
              <textarea
                id="decoded-jwt"
                value={decoded}
                readOnly
                className="resize-none border-solid border-2 border-slate-800 h-64 p-2"
              ></textarea>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

"use client";
import Header from "@/components/Header";
import { useState } from "react";

export default function JwtDecoderPage() {
  const [decoded, setDecoded] = useState("");
  const [encoded, setEncoded] = useState("");
  const [error, setError] = useState<String | undefined>();

  const decode = () => {
    setError("");
    try {
      setDecoded(decodeURIComponent(encoded));
    } catch (e) {
      if (encoded.length === 0) {
        setError('Insert the URL')
      } else {
        setError("Invalid URL");
      }
    }
  };

  const encode = () => {
    setError("");
    try {
      setEncoded(encodeURIComponent(decoded));
    } catch (e) {
      if (encoded.length === 0) {
        setError('Insert the URL')
      } else {
        setError("Invalid URL");
      }
    }
  };

  return (
    <>
      <Header />
      <main className="min-h-screen p-16 max-w-6xl pt-4 container mx-auto">
        <div className="flex justify-center">
          <h1 className="h1 text-lg font-bold mb-6">Roxy Tools</h1>
        </div>
        <div>
          <h2 className="text-lg font-medium mb-6">URL Encode/Decode</h2>
          <div className="flex gap-4">
            <div className="flex-1 flex flex-col">
              <label htmlFor="encoded-jwt" className="mb-2">
                Encoded URL
              </label>
              <textarea
                id="encoded-jwt"
                value={encoded}
                onChange={(e) => {setError(''); setEncoded(e.target.value)}}
                className="resize-none border-solid border-2 border-slate-800 h-64 p-2"
                placeholder="https%3A%2F%2Fwww.example.com%2Fsearch%3Fquery%3Dhello%20world"
              ></textarea>
              {error && <p className="text-rose-500">{error}</p>}
              <div>
                <button
                  onClick={decode}
                  type="button"
                  className="mt-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                >
                  Decode {'->'}
                </button>
                <button type="button" onClick={() => setEncoded('')} className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700">
                  Clear
                </button>
              </div>
            </div>
            <div className="flex-1 flex flex-col">
              <label htmlFor="decoded-jwt" className="mb-2">
                Decoded URL
              </label>
              <textarea
                id="decoded-jwt"
                value={decoded}
                readOnly
                className="resize-none border-solid border-2 border-slate-800 h-64 p-2"
                placeholder="https://www.example.com/search?query=hello world"
              ></textarea>
              <div>
                <button
                  onClick={encode}
                  type="button"
                  className="mt-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                >
                  {'<-'} Encode
                </button>
                <button type="button" onClick={() => setDecoded('')} className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700">
                  Clear
                </button>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

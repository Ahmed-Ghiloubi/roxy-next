import Script from "next/script";
import "./globals.css";
import { Inter } from "next/font/google";
import MinimalistHeader from "@/components/MinimalistHeader";
import Footer from "@/components/Footer";

export const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Roxy tools - Web tools",
  description: "Productivity tools and converters",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js" async></script>
      </head>
      <body className={`${inter.className} flex min-h-full flex-col bg-neutral-950 mx-2`}>
        <MinimalistHeader />
        <div className="bg-white pb-20 min-h-[25vw]">
          {children}
        </div>
        <Footer />
      </body>
    </html>
  );
}

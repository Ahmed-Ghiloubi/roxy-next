function Circle() {
  return (
    <div className="animate-bounce bg-white dark:bg-slate-800 p-2 w-10 h-10 ring-1 ring-slate-900/5 dark:ring-slate-200/20 shadow-lg rounded-full flex items-center justify-center">
      <svg
        className="w-6 h-6 text-white"
        fill="none"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-width="2"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path d="M19 14l-7 7m0 0l-7-7m7 7V3"></path>
      </svg>
    </div>
  );
}

export default function ContactPage() {
  return (
    <main className="mx-auto max-w-7xl px-6 lg:px-8 mt-8">
      <div className="flex">
        <h1 className="h1 text-3xl">Contact us&nbsp;</h1>
        <Circle />
      </div>
      <br />
      <p>
        <b className="text-violet-700">Email:</b> ahmedghiloubi@gmail.com
        <br />
        <b className="text-lime-700">Phone:</b> +213 661 86 80 72
        <br />
        <b className="text-red-600">Github:</b>{" "}
        <a
          className="text-green-600"
          href="https://github.com/ahmed-ghiloubi"
          target="_blank"
        >
          Ahmed Ghiloubi&apos;s Github
        </a>
      </p>
    </main>
  );
}
